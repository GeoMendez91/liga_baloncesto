<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class jugadores_controller extends CI_Controller {

	public function __construct(){
		
		parent::__construct();
		$this->load->model('jugador_model');
		$this->load->model('equipo_model');
	}

	// metodos para mostrar vistas /////////////////////////////////

	public function ingreso_jugador($id)
	{
		$data = array(
			'page_title' => 'Registrar jugador',
			'view' => 'ingreso_jugadores',
			'data_view' => array()
		);
		
		$posicion = $this->jugador_model->getposicion();
		$data['posicion'] = $posicion;

		$equipo = $this->equipo_model->get_equipos();
		$data['equipo'] = $equipo;
		$data['id'] = $id;
		$this->load->view('template/main_view',$data);
		
	}
	
	public function jugadores()
	{
		$data = array(
			'page_title' => 'Jugadores',
			'view' => 'jugadores',
			'data_view' => array()
		);
		$jugadores = $this->jugador_model->get_jugador();
		$data['jugadores'] = $jugadores;
		$this->load->view('template/main_view',$data);


	}


	public function plantilla($id)
	{
		$data = array(
			'page_title' => 'Plantilla',
			'view' => 'plantilla',
			'data_view' => array()
		);
		$equipos = $this->equipo_model->get_equipo($id);
		$data['equipo'] = $equipos;
		$jugadores = $this->jugador_model->get_jugadores($id);
		$data['jugadores'] = $jugadores;
		$this->load->view('template/main_view',$data);


	}









	//metodos para ingresar //////////////////////////////////
	public function agregar_jugador(){
		if ($this->input->is_ajax_request()) {
			$data = array(
				
				'dorsal' =>  $this->input->post('dorsal'),
				'nombrej' =>  $this->input->post('nombrej'),
				'estatura' =>  $this->input->post('estatura'),
				'idposicion' =>  $this->input->post('idposicion'),
				'idequipo' =>  $this->input->post('idequipo'),
				'foto' =>  $this->input->post('foto'),
				
			);



			if ($this->jugador_model->insert_jugador($data)) {
				echo json_encode(array('success' => 1));
			}else{ 
				echo json_enconde(array('success' => 0));
			}
		}else{
				//error 404
			echo "no se puede acceder";
		}
	}
	

}

?>
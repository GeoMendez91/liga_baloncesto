<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class equipo_controller extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->load->model('equipo_model');
		$this->load->model('jugador_model');
	}

	public function equipos()
	{
	$data = array(
			'page_title' => 'inicio',
			'view' => 'equipos',
			'data_view' => array()
		);
		$equipos = $this->equipo_model->get_equipos();
		$data['equipo'] = $equipos;
		$this->load->view('template/main_view',$data);
	}

}


 ?>
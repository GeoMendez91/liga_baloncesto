<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="container my-3">
		<p class="font-weight-bold text-light">Inicio / Índice de equipo</p>
		<div class="row">
      <?php foreach ($equipo as $e) {?>
       <div class="col-md-3 my-2">
         <div class="card" style="width: 18rem;">
           <img class="card-img-top" src="<?php echo base_url('assets/img/') ?><?php echo $e->logo ?>" alt="Card image cap">
           <div class="card-body">
            <p class="card-text text-center"><strong class="name"><?= $e->nombre ?></strong></p>
            <hr>
            <p class="card-text" style="height: 40px"><strong>Estadio: </strong><?= $e->nombre_es ?></p>
            <hr>
            <div class="btn-group" role="group" aria-label="Basic example">
              <a href="<?php echo base_url('jugadores_controller/plantilla/').$e->idequipo ?>"><button type="button" class="btn btn-danger">Plantilla</button></a>
              <button type="button" class="btn btn-danger">Calendario</button>
              <button type="button" class="btn btn-danger">Líderes</button>
            </div>
            
            
          </div>
        </div>
      </div>

    <?php } ?>
  </div>
</div>

</body>
</html>
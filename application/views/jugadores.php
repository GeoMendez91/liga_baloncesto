<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div class="container my-5">
        <div >
            <table class="table table-light table-striped " id="tabla">
                <thead class="bg-warning">
                    <tr>
                        <th class="text-center tipo"></th> 
                        <th class="text-center tipo">Dorsal</th> 
                        <th class="text-center tipo">Nombre</th> 
                        <th class="text-center tipo">Estatura</th> 
                        <th class="text-center tipo">Posición</th> 
                        <th class="text-center tipo">Equipo</th>
                        
                    </tr>
                </thead>
                <tbody >
                    <?php foreach ($jugadores as $j) {?>
                        <tr>
                            <td align="center" class="align-middle"><img src="<?php echo base_url('assets/img/').$j->foto ?>" alt="" height="50px"></td>
                            <td align="center" class="align-middle"><?= $j->dorsal ?></td>
                            <td align="center" class="align-middle"><?= $j->nombrej ?></td>
                            <td align="center" class="align-middle"><?= $j->estatura ?></td>
                            <td align="center" class="align-middle"><?= $j->posicion ?></td>
                            <td align="center" class="align-middle"><?= $j->nombre ?></td>
                            
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>

    
</body>
</html>
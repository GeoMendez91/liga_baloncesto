<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>

    <div class="container my-5 ">
        <?php foreach ($equipo as $e) { ?>
            <a href="<?php echo base_url('jugadores_controller/ingreso_jugador/').$e->idequipo ?>" class="b-right"><button class="btn btn-danger tipo">Agregar jugador</button></a>
            <table class="table-secondary col-12">
                
                <tbody>
                    
                        <tr>
                            <td  class="align-middle"><img src="<?php echo base_url('assets/img/') . $e->logo ?>" alt="" height="200px"></td>
                            <td class="align-middle tipo"><h1><?= $e->nombre ?></h1></td>
                            <td align="center" class="align-middl tipo"><h2><?php if ($e->puntos == null){
                                echo "0 Puntos";
                            }else{ echo $e->puntos." Puntos";  } ?></h2></td>

                        </tr>
                    
                </tbody>
            </table>
       <?php } ?>

       
            <table class="table table-light table-striped ">
                <thead class="bg-warning">
                    <tr>
                        <th class="text-center tipo"></th>
                        <th class="text-center tipo">Dorsal</th>
                        <th class="text-center tipo">Nombre</th>
                        <th class="text-center tipo">Estatura</th>
                        <th class="text-center tipo">Posición</th>


                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($jugadores as $j) { ?>
                        <tr>
                            <td align="center" class="align-middle"><img src="<?php echo base_url('assets/img/') . $j->foto ?>" alt="" height="50px"></td>
                            <td align="center" class="align-middle"><?= $j->dorsal ?></td>
                            <td align="center" class="align-middle"><?= $j->nombrej ?></td>
                            <td align="center" class="align-middle"><?= $j->estatura . " mts" ?></td>
                            <td align="center" class="align-middle"><?= $j->posicion ?></td>

                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        
    </div>


</body>

</html>
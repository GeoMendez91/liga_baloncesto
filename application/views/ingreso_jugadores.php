<!DOCTYPE html>
<html>

<head>
	<title></title>
</head>

<body>
	<div class="container my-5">
		<div class="row">
			<div class="col-md-6">

				<img style="width: 700px" src="<?php echo base_url('assets/img/jugador2.gif') ?>">
			</div>

			<div class="col-md-6">
				<form action="<?php echo base_url('jugadores_controller/agregar_jugador') ?>" method="post">
					<table align="right" style="width: 350px">
						<tbody>
							<tr>
								<td class="text-warning font-weight-bold tipo">Dorsal</td>
							</tr>
							<tr>
								<td>
									<input class="form-control" id="dorsal" type="number" placeholder="" required="">
									<input type="hidden" id="idequipo" value="<?php echo $id ?>">
								</td>
							</tr>
							<tr style="height: 20px"></tr>
							<tr>
								<td class="text-warning font-weight-bold tipo">Nombre</td>
							</tr>
							<tr>
								<td>
									<input class="form-control" id="nombrej" type="text" required="">
								</td>
							</tr>
							<tr style="height: 20px"></tr>
							<tr>
								<td class="text-warning font-weight-bold tipo">Estatura</td>
							</tr>
							<tr>
								<td>
									<input class="form-control" type="number" id="estatura" step="0.01" placeholder="" required="">
								</td>
							</tr>
							<tr style="height: 20px"></tr>
							<tr>
								<td class="text-warning font-weight-bold tipo">Posición</td>
							</tr>
							<tr>
								<td>
									<select id="idposicion" class="form-control" required="">>
										<option value="">--Seleccione una opción--</option>
										<?php foreach ($posicion as $p) { ?>
											<option value="<?= $p->idposicion ?>"><?= $p->posicion ?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr style="height: 20px"></tr>
							
							<tr>
								<td>
									<div class="form-group" style="width: 350px">
										<label class="text-warning font-weight-bold tipo">Fotografía</label>
										<div class="input-group">
											<span class="input-group-btn">
												<span class="btn btn-danger btn-file">
													Browse… <input type="file" id="imgInp" required="">
												</span>
											</span>
											<input type="text" id="foto" class="form-control" readonly>
										</div>
										<img id='img-upload' />
									</div>
								</td>
							</tr>
						</tbody>
					</table>







				</div>
			</div>
			<div class="col-md-12 my-5">
				<input type="button" class="btn btn-danger btn-lg btn-block" value="Registrar jugador" id="registar-jugador" />
			</div>

		</form>
	</div>

</body>

</html>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>	
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/image.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/agregar.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/datatables.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.nice-select.js"></script>
	

	<script type="text/javascript">
		$(document).ready(function () {
			$('#tabla').DataTable();
			$('.dataTables_length').addClass('bs-select');
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('select').niceSelect();
		});
	</script>

</body>
</html>
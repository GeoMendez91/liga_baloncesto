<!DOCTYPE html>
<html>
<head>
	<title><?= $page_title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/estilos.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/nice-select.css');?>">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>	
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.3.1.min') ?>"></script>
	<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet"> 
</head >
<body style="background-color: #140126">
	<?php $this->load->view('template/header_view') ?>
	<?php $this->load->view($view,$data_view) ?>
	<?php $this->load->view('template/footer_view') ?>

</body>
</html>
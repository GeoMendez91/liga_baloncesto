<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class jugador_model extends CI_Model {

	

	public function get_jugadores($equipo)
	{
		$this->db->select('j.idjugador,j.dorsal,j.nombrej,j.estatura,p.posicion,j.foto');
		$this->db->from('jugador j');
			$this->db->join('posicion p','p.idposicion = j.idposicion');
			
    $this->db->where('idequipo = '.$equipo);
	$j = $this->db->get();
	return $j->Result();
	}

	public function get_jugador()
	{
	$this->db->select('j.idjugador,j.dorsal,j.nombrej,j.estatura,p.posicion,e.nombre,j.foto');
	$this->db->from('jugador j');
		$this->db->join('posicion p','p.idposicion = j.idposicion');
		$this->db->join('equipo e','e.idequipo = j.idequipo');
	$j = $this->db->get();
	return $j->Result();
	}

	public function get_estadios(){

    }
    
    public function getposicion(){
      $p = $this->db->get('posicion');
      return $p->result();
		}
		
		public function insert_jugador($data){

			return ($this->db->insert('jugador',$data)) ? true:false;

		}



}


?>